import React, { Component } from "react";
import { connect } from "react-redux";
import * as actions from "../actions";
import globalStyles from "../styles/global-styles";

const fetch = require("node-fetch");

var _ = require("underscore");

// const httpLink = createHttpLink({
//     uri: 'http://13.233.142.4/',
//     headers:{
//         "Session-Token": 'lsjdhfGHDHKUYWEBNBMkuLJHKSDGYUIEBKJHkjhkjhDSBDKBK'
//     },
//     fetch:fetch
//   })

// const client = new ApolloClient({
//   link: httpLink,
//   cache: new InMemoryCache()
// })

class Main extends Component {
    constructor(props) {
        super(props);
        this.deferredPrompt = null;
        this.state = {
            currentRoute: "",
            showInstallPrompt: false
        };
    }

    componentDidMount() {
        window &&
            window.addEventListener("beforeinstallprompt", e => {
                // Prevent the mini-infobar from appearing on mobile
                e.preventDefault();
                // Stash the event so it can be triggered later.
                global.deferredPrompt = e;
                // Update UI notify the user they can install the PWA
                this.setState({
                    showInstallPrompt: true
                });
            });
    }

    componentWillUnmount() {}

    renderChildrens() {
        const { location } = this.props;
        const that = this;
        let width = "80%";
        let marginLeft = "20%";
        let position = "fixed";
        if (location && location.pathname == "/") {
            width = "100%";
            marginLeft = "0%";
            position = "relative";
        }
        return React.Children.map(this.props.children, child => {
            const clonedProps = {
                ...child.props
            };
            return (
                <div style={{ overflow: "hidden" }}>
                    {React.cloneElement(child, clonedProps)}
                </div>
            );
        });
    }

    render() {
        globalStyles();
       // console.log(this.props);
        return (
            <div>
                {this.renderChildrens()}
                {this.state.showInstallPrompt && this.props.generalData.pwaInstall ? this.props.isMobile ? (
                    <div
                        style={{
                            position: "absolute",
                            boxShadow: "rgba(0, 0, 0, 0.1) 0px 0px 9px 2px",
                            width: "100%",
                            bottom: 0,
                            display: "flex",
                            flexDirection: "column",
                            backgroundColor: "#fff"
                        }}
                    >
                        <div
                            style={{
                                display: "flex",
                                flexDirection: "row",
                                padding: "15px 20px",
                                paddingBottom: 5,
                                justifyContent: "space-between",
                                alignItems: "flex-start"
                            }}
                        >
                            <img
                                src="https://i.imgur.com/93l5bZW.png"
                                style={{ width: 70 }}
                            />
                            <div
                                style={{
                                    flexDirection: "column",
                                    flex: 1,
                                    paddingLeft:5,
                                    paddingTop:5,
                                    display: "flex"
                                }}
                            >
                                <span
                                    style={{
                                        color: "#000",
                                        fontSize: 20,
                                        fontWeight: 500
                                    }}
                                >
                                    Token App Lite
                                </span>
                                <span
                                    style={{ color: "#00000050", fontSize: 14 }}
                                >
                                    Token.App.lite
                                </span>
                            </div>
                            <img
                                onClick={()=>{
                                    this.props.updatePwa(false);
                                }}
                                src="https://i.imgur.com/FaLGd6M.png"
                                style={{
                                    width: 17.5,
                                    marginTop: 10,
                                    objectFit: "contain"
                                }}
                            />
                        </div>
                        <div
                            style={{
                                justifyContent: "flex-end",
                                display: "flex"
                            }}
                        >
                            <div
                                onClick={()=>{
                                   this.props.updatePwa(false);
                                   global.deferredPrompt.prompt();
                                }}
                                style={{
                                    backgroundColor: "#59af50",
                                    borderRadius: 5,
                                    textAlign: "center",
                                    color: "#fff",
                                    padding: 10,
                                    marginBottom: 10,
                                    marginRight: 10,
                                    width: "50%"
                                }}
                            >
                                ADD TO HOMESCREEN
                            </div>
                        </div>
                    </div>
                ):(
                    <div
                        style={{
                            position: "absolute",
                            boxShadow: "rgba(0, 0, 0, 0.1) 0px 0px 9px 2px",
                            borderTopLeftRadius:5,
                            borderBottomLeftRadius:5,
                            width: "30%",
                            top: 60,
                            right:0,
                            zIndex:999,
                            display: "flex",
                            flexDirection: "column",
                            backgroundColor: "#fff"
                        }}
                    >
                        <div
                            style={{
                                display: "flex",
                                flexDirection: "row",
                                padding: "15px 20px",
                                paddingBottom: 5,
                                justifyContent: "space-between",
                                alignItems: "flex-start"
                            }}
                        >
                            <img
                                src="https://i.imgur.com/93l5bZW.png"
                                style={{ width: 70 }}
                            />
                            <div
                                style={{
                                    flexDirection: "column",
                                    flex: 1,
                                    paddingLeft:5,
                                    paddingTop:5,
                                    display: "flex"
                                }}
                            >
                                <span
                                    style={{
                                        color: "#000",
                                        fontSize: 20,
                                        fontWeight: 500
                                    }}
                                >
                                    Token App Lite
                                </span>
                                <span
                                    style={{ color: "#00000050", fontSize: 14 }}
                                >
                                    token.app.lite
                                </span>
                            </div>
                            <img
                                onClick={()=>{
                                    this.props.updatePwa(false);
                                }}
                                src="https://i.imgur.com/FaLGd6M.png"
                                style={{
                                    width: 17.5,
                                    marginTop: 10,
                                    objectFit: "contain"
                                }}
                            />
                        </div>
                        <div
                            style={{
                                justifyContent: "flex-end",
                                display: "flex"
                            }}
                        >
                            <div
                                onClick={()=>{
                                   this.props.updatePwa(false);
                                   global.deferredPrompt.prompt();
                                }}
                                style={{
                                    backgroundColor: "#59af50",
                                    borderRadius: 5,
                                    textAlign: "center",
                                    color: "#fff",
                                    padding: 10,
                                    marginBottom: 10,
                                    marginRight: 10,
                                    width: "50%"
                                }}
                            >
                                ADD TO HOMESCREEN
                            </div>
                        </div>
                    </div>
                ) : null}
            </div>
        );
    }
}

function mapStateToProps(state) {
    console.log(state);
    return {
        isMobile: state.utils && state.utils.is_mobile,
        generalData: state.generalData,
    };
}

export default connect(mapStateToProps, actions)(Main);
