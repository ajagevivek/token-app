import React from "react";
import { connect } from "react-redux";
class Dashboard extends React.Component {
    constructor(props){
        super(props);

    }
    render() {
        
        return (
            <div>
                Dashboard
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        isMobile: state.utils.is_mobile
    };
};

export default connect(mapStateToProps, null)(Dashboard);
